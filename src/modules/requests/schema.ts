import * as mongoose from 'mongoose';
import { Modification } from '../common/model';

const Schema = mongoose.Schema;

const schema = new Schema({
    date: Date,
    method: String,
    data: [],
    is_deleted: {
        type: Boolean,
        default: false
    },
    modification: [Modification]
});

export default mongoose.model('requests', schema);