import { IRequest } from './model';
import requests from './schema';

export default class RequestService {
    
    public createRequest(req_params: IRequest, callback: any) {
        const _session = new requests(req_params);
        _session.save(callback);
    }

    public filterRequest(query: any, callback: any) {
        requests.findOne(query, callback);
    }

    public allRequests(callback: any) {
        requests.find(callback);
    }

    public updateRequest(req_params: IRequest, callback: any) {
        const query = { _id: req_params._id };
        requests.findOneAndUpdate(query, req_params, callback);
    }
    
    public deleteRequest(_id: String, callback: any) {
        const query = { _id: _id };
        requests.deleteOne(query, callback);
    }

}