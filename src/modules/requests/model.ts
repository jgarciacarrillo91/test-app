import { Modification } from "../common/model";

export interface IRequest {
    _id?: String;
    date: Date;
    method: String;
    data: [];
    is_deleted?: Boolean;
    modification: Modification[]
}