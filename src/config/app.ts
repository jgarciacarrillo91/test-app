//src/config/app.ts
import * as express from "express";
import * as mongoose from 'mongoose';
import environment from "../environment";
import { ApiRoutes } from "../routes/api_routes";
import { ReqRoutes } from "../routes/request_routes";
import { ExpRoutes } from "../routes/export_routes";
import { CommonRoutes } from "../routes/common_routes";

class App {
   
   public app: express.Application;
   public mongoUrl: string = environment.getPathHost()+''+environment.getDBName();

   private api_routes: ApiRoutes = new ApiRoutes();
   private req_routes: ReqRoutes = new ReqRoutes ();
   private exp_routes: ExpRoutes = new ExpRoutes ();
   private common_routes: CommonRoutes = new CommonRoutes();

   constructor() {
      this.app = express();
      this.config();
      this.mongoSetup();
      this.api_routes.route(this.app);
      this.req_routes.route(this.app);
      this.exp_routes.route(this.app);
      this.common_routes.route(this.app);
   }
   
   private config(): void {
      this.app.use(express.json());
      this.app.use(express.urlencoded({ extended: false }));
   }

   private mongoSetup(): void {
      mongoose.connect(this.mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false });
   }
}
export default new App().app;
