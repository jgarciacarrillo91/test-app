import { Request, Response } from 'express';
import { insufficientParameters, mongoError, successResponse, failureResponse } from '../modules/common/service';
import { IRequest } from '../modules/requests/model';
import RequestService from '../modules/requests/service';

export class RequestController {

    private req_service: RequestService = new RequestService();

    public create_request(req: Request, res: Response) {
        if (req.complete === true) {
            const req_params: IRequest = {
                date: new Date(Date.now()), 
                method: req.method,
                data: req['data'],
                modification: [{
                    modified_on: new Date(Date.now()),
                    modified_by: null,
                    modification: 'New request created'
                }]
            };
            this.req_service.createRequest(req_params, (err: any, request_data: IRequest) => {
                if (err) {
                    mongoError(err, res);
                } 
            });
        } else {
            // error response if some fields are missing in request body
            insufficientParameters(res);
        }
    }

    public get_request(req: Request, res: Response) {
        if (req.params.id) {
            const request_filter = { _id: req.params.id };
            this.req_service.filterRequest(request_filter, (err: any, request_data: IRequest) => {
                if (err) {
                    mongoError(err, res);
                } else {
                    successResponse('get request successfull', request_data, res);
                }
            });
        } else {
            insufficientParameters(res);
        }
    }

    
    public get_requests(res: Response) {
        this.req_service.allRequests((err:any, request_data: IRequest) => {
            if (err) {
                mongoError(err, res);
            } else {
                successResponse('get requests successfull', request_data, res);
            }
        });
       
    }

    public update_request(req: Request, res: Response) {
        if (req.params.id && req.body.date || req.body.method || req.body.data) {
            const request_filter = { _id: req.params.id };
            this.req_service.filterRequest(request_filter, (err: any, request_data: IRequest) => {
                if (err) {
                    mongoError(err, res);
                } else if (request_data) {
                    request_data.modification.push({
                        modified_on: new Date(Date.now()),
                        modified_by: null,
                        modification: 'Request data updated'
                    });
                    const req_params: IRequest = {
                        _id: req.params.id,
                        date: req.body.date ? req.body.date : request_data.date,
                        method: req.body.method ? req.body.method : request_data.method,
                        data: req.body.data ? req.body.data : request_data.data,
                        is_deleted: req.body.is_deleted ? req.body.is_deleted : request_data.is_deleted,
                        modification: request_data.modification
                    };
                    this.req_service.updateRequest(req_params, (err: any) => {
                        if (err) {
                            mongoError(err, res);
                        } else {
                            successResponse('update request successfull', null, res);
                        }
                    });
                } else {
                    failureResponse('invalid request', null, res);
                }
            });
        } else {
            insufficientParameters(res);
        }
    }

    public delete_request(req: Request, res: Response) {
        if (req.params.id) {
            this.req_service.deleteRequest(req.params.id, (err: any, delete_details) => {
                if (err) {
                    mongoError(err, res);
                } else if (delete_details.deletedCount !== 0) {
                    successResponse('delete request successfull', null, res);
                } else {
                    failureResponse('invalid request', null, res);
                }
            });
        } else {
            insufficientParameters(res);
        }
    }
}