import { Request, Response } from 'express';
import { successResponse, failureResponse } from '../modules/common/service';

export class ExportController {

    public export_data(req: Request, res: Response){
        if (req.body.data) {
            var csvContent = btoa(req.body.data);
            successResponse('Successfull', csvContent, res);
        }else{
            failureResponse('No data to export', null, res);
        }
    }
}