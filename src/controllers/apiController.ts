import { Request, Response } from 'express';
import { RequestController } from './requestController';
import { successResponse } from '../modules/common/service';

import fetch = require("node-fetch");

export class ApiController {

    public url_api = 'https://jsonplaceholder.typicode.com/';
    private req_controller: RequestController = new RequestController();

    public async get_users(req: Request, res: Response) {
        const users = await fetch(this.url_api+'users').then((response) => {return response.json()})
        .then((data) => { return data;}).catch( (error) => { res.send(error)});
        req["data"] = users;
        this.req_controller.create_request(req, res);
        successResponse('Successfull', users, res);
    }

    public async get_posts(req: Request, res: Response) {
        const posts = await fetch(this.url_api+'posts') .then((response) => { return response.json();})
        .then((data) => { return data;}).catch( (error) => { res.send(error)});
        req["data"] = posts;
        this.req_controller.create_request(req, res);
        successResponse('Successfull', posts, res);
    }

    //we get the user's albums to get the photos
    public async get_photos_user(req: Request, res: Response){
        const photos = await fetch(this.url_api+`albums/?userId=${req.params.userid}`)
        .then((response) => response.json()).then((data) => {
            return Promise.all(data.map(item => {
                return fetch(this.url_api+`photos/?albumId=${item.id}`)
                .then(data => data.json())
                .then(data => { return data })
            }));
        }).then(data => { return data }).catch( (error) => { res.send(error);});
        req["data"] = photos;
        this.req_controller.create_request(req, res);
        successResponse('Successfull', photos, res);
    }
}