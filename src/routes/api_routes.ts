//src/routes/test_routes.ts
import {Application, Request, Response } from 'express';
import { ApiController } from '../controllers/apiController';


export class ApiRoutes {
   private api_controller: ApiController = new ApiController();
   
   public route(app: Application) {
      
      app.get('/api/users', (req: Request, res: Response) => {
         this.api_controller.get_users(req, res);
      });

      app.get('/api/posts', (req: Request, res: Response) => {
         this.api_controller.get_posts(req, res);
      });

      app.get('/api/photos/:userid', (req: Request, res: Response) => {
         this.api_controller.get_photos_user(req, res);
      });

   }
}