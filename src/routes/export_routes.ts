//src/routes/test_routes.ts
import {Application, Request, Response } from 'express';
import { ExportController } from '../controllers/exportController';


export class ExpRoutes {
   private exp_controller: ExportController = new ExportController();
    
   public route(app: Application) {
      
      app.get('/api/export', (req: Request, res: Response) => {
         this.exp_controller.export_data(req, res);
      });
   }
}