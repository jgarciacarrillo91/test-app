import { Application, Request, Response } from 'express';
import { RequestController } from '../controllers/requestController';

export class ReqRoutes {

    private req_controller: RequestController = new RequestController();

    public route(app: Application) {
        
        app.post('/api/request', (req: Request, res: Response) => {
            this.req_controller.create_request(req, res);
        });

        app.get('/api/requests', (req: Request, res: Response) => {
            this.req_controller.get_requests(res);
        });

        app.get('/api/request/:id', (req: Request, res: Response) => {
            this.req_controller.get_request(req, res);
        });

        app.put('/api/request/:id', (req: Request, res: Response) => {
            this.req_controller.update_request(req, res);
        });

        app.delete('/api/request/:id', (req: Request, res: Response) => {
            this.req_controller.delete_request(req, res);
        });

    }
}